#!/bin/bash

validate_port() {
    local port=$1
    if ! [[ "$port" =~ ^[0-9]+$ ]] || [ "$port" -lt 1 ] || [ "$port" -gt 65535 ]; then
        echo "Invalid port number. Please enter a valid port."
    fi
}

manage_listed_jobs(){
    echo "===================================="
    echo "      List of scheduled jobs        "
    echo "===================================="
    local i=0
    # Use a temporary file to store the modified crontab
    temp_file=$(mktemp)

    # Loop through the crontab, incrementing the index
    while read -r line; do
        echo "  [${i}] - ${line}"
        ((i++))
    done < <(crontab -l)

    read -p "Enter the index of the scheduled backup to delete: " INDEX
    echo "===================================="
    if [[ ! $INDEX =~ ^[0-9]+$ ]] || [ "$INDEX" -ge "$i" ]; then
        echo "Invalid index. Returning to main menu."
        return
    fi

    # Copy crontab to the temporary file excluding the selected line
    crontab -l | awk -v index="$INDEX" 'NR!=index+1' | crontab -
    echo "Scheduled backup deleted successfully."
}

echo "Welcome to SafetyNety - Database Backup and Recovery"

while true; do
    echo "===================================="
    echo "           Menu Options             "
    echo "===================================="
    echo "  [1] - Setup RSA"
    echo "  [2] - Schedule backup"
    echo "  [3] - Schedule rsync"
    echo "  [4] - Recovery"
    echo "  [5] - Manage Scheduled jobs"
    echo "  [6] - Exit"
    echo "===================================="
    read -p "Choose an action (1-6): " ACTION
    ACTION=${ACTION:-"1"}
    
    case "$ACTION" in
        "1")
            echo "Selected action: Setup RSA"
            ssh_key_path="$HOME/.ssh/id_rsa"

            # Generate SSH key pair if it doesn't exist
            if [ ! -f "$ssh_key_path" ]; then
                ssh-keygen -t rsa -b 2048 -f "$ssh_key_path" -N ""
                echo "Your public key:"
                cat "${ssh_key_path}.pub"
            else
                echo "SSH key already exists at $ssh_key_path"
            fi

            # Prompt for the remote server's user and hostname
            read -p "Enter the remote server's username: " remote_user
            read -p "Enter the remote server's hostname or IP address: " remote_host

            # Copy the public key to the remote server
            ssh-copy-id "${remote_user}@${remote_host}"
        ;;
        "2")
            source ./postgresql/backup_functions.sh
            echo "Selected action: Schedule backup"
            init

            backup_schedule=$(set_backup_schedule)
            echo "Entered schedule: $backup_schedule"

            backup_local_path=$(set_backup_directory)
            echo "Entered backup directory: $backup_local_path"

            backup_db_port=$(set_database_port)
            echo "Entered backup DB Port: $backup_db_port"

            backup_db_name=$(select_database "$backup_db_port")
            echo "Entered backup DB Name: $backup_db_name"

            schedule_backup_job "$backup_schedule" "$backup_db_port" "$backup_db_name" "$backup_local_path"
        ;;
        "3")
            # source rsync_functions.sh
            echo "Selected action: Schedule rsync"
            echo "Not Yet implemented."

            # rsync_schedule=$(set_rsync_schedule)
            # local_path=$(set_local_path)
            # rsync_db_port=$(set_rsync_db_port)
            # rsync_db_name=$(select_rsync_database "$rsync_db_port")
            # remote_user=$(set_remote_user)
            # remote_host=$(set_remote_host)
            # remote_path=$(set_remote_path)
            #
            # schedule_rsync_job "$rsync_schedule" "$local_path" "$rsync_db_port" "$rsync_db_name" "$remote_user" "$remote_host" "$remote_path"        
        ;;
        "4")
            echo "Recovery"
            exit 0
        ;;
        "5")
            echo "Manage Scheduled Jobs"
            manage_listed_jobs
        ;;
        "6")
            echo "Exiting the program."
            exit 0
        ;;
        *)
            echo "Invalid action. Please choose a valid action."
        ;;
    esac
done
