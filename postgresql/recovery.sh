#!/bin/bash

# Configuration
DB_USER="your_postgresql_user"
DB_PASSWORD="your_postgresql_password"
DB_NAME="your_postgresql_database"
BACKUP_DIR="/path/to/backup/directory"

# Prompt for backup file
read -p "Enter the backup file to restore: " BACKUP_FILE

# Perform database recovery
pg_restore -U $DB_USER -h localhost -p 5432 -W -d $DB_NAME $BACKUP_DIR/$BACKUP_FILE

# Check for success
if [ $? -eq 0 ]; then
    echo "Recovery completed successfully."
else
    echo "Recovery failed. Check the error message."
fi
