#!/bin/bash

database_port=$1
database_name=$2
backup_dir=$3

current_date=$(date +%Y%m%d)
current_date_time=$(date +%Y%m%d%H%M%S)
log_file="$backup_dir/backup_log.log"

log() {
    local message="$1"
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $message" | tee -a "$log_file"
}

touch "$log_file" || { echo "Error: Unable to create the log file. Exiting."; exit 1; }

log "Starting backup script..."

newest_file=$(ls -t "$backup_dir" | grep "safetynety_backup_${HOSTNAME}_${database_port}_${database_name}_" | head -n 1)
log "Newest file identified: $newest_file"

log "Deleting old files..."
prefix="safetynety_backup_${HOSTNAME}_${database_port}_${database_name}_*.sql"
find "$backup_dir" -type f -name $prefix ! -newer "$backup_dir/$newest_file" -not -name "$newest_file" -delete

backup_file="$backup_dir/safetynety_backup_${HOSTNAME}_${database_port}_${database_name}_${current_date_time}.sql"
log "Performing backup..."
pg_dump -p "$database_port" -d "$database_name" > "$backup_file" && log "Backup completed"

log "Backup Info:"
log "  - Hostname: ${HOSTNAME}"
log "  - Database: $database_name"
log "  - Port: $database_port"
log "  - Backup File: $backup_file"
