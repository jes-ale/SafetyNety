#!/bin/bash

set_rsync_schedule() {
    local rsync_schedule
    read -p "Enter cron schedule for automated rsync (default every day at 6 a.m.: 0 6 * * *): " rsync_schedule
    rsync_schedule=${rsync_schedule:-"0 6 * * *"}
    echo "$rsync_schedule"
}
set_local_path() {
    local local_path
    read -p "Enter local path (default: ~/.backups_safetynety): " local_path
    local_path=${local_path:-"$HOME/.backups_safetynety"}
    echo "$local_path"
}

set_rsync_db_port() {
    local rsync_db_port
    read -p "Enter database port (default: 5432): " rsync_db_port
    rsync_db_port=${rsync_db_port:-"5432"}
    validate_port "$rsync_db_port"
    echo "$rsync_db_port"
}

select_rsync_database() {
    local rsync_db_port=$1
    local rsync_db_name

    while true; do
        db_list=$(psql -p $rsync_db_port -l -t | cut -d \| -f 1 | grep -n "")

        echo "Available databases:"
        echo "$db_list"

        read -p "Enter the index of the database to connect to: " rsync_db_name

        if [ -z "$rsync_db_name" ]; then
            echo "Database name cannot be empty. Please enter a valid database name"
        else
            break
        fi
    done

    echo "$rsync_db_name"
}

set_remote_user() {
    local remote_user
    while true; do
        read -p "Enter the remote server username: " remote_user
        if [ -z "$remote_user" ]; then
            echo "Username cannot be empty. Please enter a valid username."
        else
            break
        fi
    done

    echo "$remote_user"
}

set_remote_host() {
    local remote_host
    while true; do
        read -p "Enter the remote server's hostname or IP address: " remote_host
        if [ -z "$remote_host" ]; then
            echo "Hostname or IP address cannot be empty. Please enter a valid hostname or IP address."
        else
            break
        fi
    done

    echo "$remote_host"
}

set_remote_path() {
    local remote_path
    read -p "Enter remote path (default: ~/.backups_safetynety): " remote_path
    remote_path=${remote_path:-"$HOME/.backups_safetynety"}
    echo "$remote_path"
}

schedule_rsync_job() {
    local rsync_schedule=$1
    local local_path=$2
    local rsync_db_port=$3
    local rsync_db_name=$4
    local remote_user=$5
    local remote_host=$6
    local remote_path=$7

    local current_hostname=$(hostname)
    local rsync_files="${local_path}/safetynety_backup_${current_hostname}_${rsync_db_port}_${rsync_db_name}_*.sql"

    (crontab -l ; echo "${rsync_schedule} rsync -avz ${rsync_files} ${remote_user}@${remote_host}:${remote_path}/") | crontab -
}
