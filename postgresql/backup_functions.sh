#!/bin/bash

# Function to check if a command is available
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Function to install PostgreSQL client and dependencies
install_postgresql() {
    echo "Installing PostgreSQL client and dependencies..."
    sudo apt-get update
    sudo apt-get install -y postgresql-client
}

init() {
    # Check if pg_dump command is available
    if ! command_exists pg_dump; then
        install_postgresql
        # Check again after installation
        if ! command_exists pg_dump; then
            echo "Error: Failed to install PostgreSQL client."
            exit 1
        fi
    fi

    # Check if pg_basebackup command is available
    if ! command_exists pg_basebackup; then
        install_postgresql
        # Check again after installation
        if ! command_exists pg_basebackup; then
            echo "Error: Failed to install PostgreSQL client."
            exit 1
        fi
    fi

    # Check if rsync command is available
    if ! command_exists rsync; then
        echo "Installing rsync..."
        sudo apt-get update
        sudo apt-get install -y rsync
        # Check again after installation
        if ! command_exists rsync; then
            echo "Error: Failed to install rsync."
            exit 1
        fi
    fi

    echo "All dependencies are ready."
}

# Function to set backup schedule
set_backup_schedule() {
    local backup_schedule
    read -p "Enter cron schedule for automated backup (default every day at midnight: 0 0 * * *): " backup_schedule
    backup_schedule=${backup_schedule:-"0 0 * * *"}
    echo "$backup_schedule"
}

# Function to set backup directory
set_backup_directory() {
    local backup_local_path
    read -p "Enter backup directory (default: ~/.backups_safetynety): " backup_local_path
    backup_local_path=${backup_local_path:-"$HOME/.backups_safetynety"}

    if [ ! -d "$backup_local_path" ]; then
        echo "Creating backup directory: $backup_local_path"
        mkdir -p "$backup_local_path"

        if [ $? -ne 0 ]; then
            echo "Error creating backup directory. Exiting."
            exit 1
        fi
    fi

    echo "$backup_local_path"
}

# Function to set database port
set_database_port() {
    local backup_db_port
    read -p "Enter database port (default: 5432): " backup_db_port
    backup_db_port=${backup_db_port:-"5432"}
    validate_port "$backup_db_port"
    echo "$backup_db_port"
}

# Function to select a database
select_database() {
    local db_list
    local backup_db_port=$1

    while true; do
        db_list=$(psql -p $backup_db_port -l -t | cut -d \| -f 1 | grep -n "")

        { echo "Available databases:"; echo "$db_list"; } > /dev/tty

        read -p "Enter the name of the database to connect to: " backup_db_name

        if [ -z "$backup_db_name" ]; then
            { echo "Database name cannot be empty. Please enter a valid database name." > /dev/tty; }
        else
            break 
        fi
    done

    echo "$backup_db_name"
}


# Get the directory of the backup_functions.sh script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Function to schedule backup job with file cleanup
schedule_backup_job() {
    local current_hostname=$(hostname)

    local current_dir="$SCRIPT_DIR"
    local script_file="backup_script.sh"

    local backup_schedule=$1

    local backup_db_port=$2
    local backup_db_name=$3
    local backup_local_path=$4

    chmod +x "$SCRIPT_DIR/$script_file"

    local cron_command="$backup_schedule /bin/bash ${SCRIPT_DIR}/${script_file} $backup_db_port $backup_db_name $backup_local_path"

    (crontab -l ; echo -e "$cron_command") | crontab -

    if [ $? -eq 0 ]; then
        echo "Automated backup scheduled successfully."
    else
        echo "Automated backup scheduling failed."
    fi
}
